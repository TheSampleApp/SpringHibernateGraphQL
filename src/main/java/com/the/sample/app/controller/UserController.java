package com.the.sample.app.controller;

import com.the.sample.app.model.User;
import com.the.sample.app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
public class UserController {
    private final UserService userService;
    @QueryMapping
    public Optional<User> findById(@Argument Long id) {
        return userService.findById(id);
    }
    @QueryMapping
    public Optional<User> findByEmail(@Argument String email) {
        return userService.findByEmail(email);
    }
    @QueryMapping
    public List<User> findAll(@Argument int page, @Argument int pageSize) {
        return userService.findAll(page, pageSize);
    }
    @MutationMapping
    public User createUser(@Argument String fullName, @Argument String email){
        User user = User.builder().fullName(fullName).email(email).build();
        userService.save(user);
        return user;
    }
    @MutationMapping
    public User updateUser(@Argument Long id,@Argument String fullName, @Argument String email){
        User user = User.builder().id(id).fullName(fullName).email(email).build();
        userService.save(user);
        return user;
    }
    @MutationMapping
    public Boolean deleteUser(@Argument Long id){
        try{
            userService.deleteById(id);
        }catch (Exception ex){
            return false;
        }
        return true;
    }
}
